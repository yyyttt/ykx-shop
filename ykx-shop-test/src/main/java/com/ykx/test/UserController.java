package com.ykx.test;

import com.alibaba.dubbo.config.annotation.Reference;
import com.ykx.shop.model.base.RspValue;
import com.ykx.shop.user.facade.ReqAddUser;
import com.ykx.shop.user.facade.UserFacade;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@Api("userController相关api")
public class UserController {

    @Reference
    private UserFacade userFacade;
    @ApiOperation("注册用户")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path",name="userName",dataType="String",required=true,value="用户的姓名",defaultValue="zhangsan"),
            @ApiImplicitParam(paramType = "path",name="pwd",dataType="String",required=true,value="用户的密码",defaultValue="111"),
            @ApiImplicitParam(paramType = "path",name="age",dataType="int",required=true,value="用户的年龄",defaultValue="10")
    })
    @GetMapping("/{userName}/{pwd}/{age}")
    public RspValue saveUser(@PathVariable String userName, @PathVariable String pwd, @PathVariable int age){
        RspValue rsp =new RspValue();
        ReqAddUser reqAddUser = new ReqAddUser();
        reqAddUser.setAge(age);
        reqAddUser.setPwd(pwd);
        reqAddUser.setUserName(userName);
        rsp = userFacade.addUser(reqAddUser);
        return rsp;
    }


}
