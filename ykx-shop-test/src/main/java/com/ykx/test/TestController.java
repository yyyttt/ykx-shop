package com.ykx.test;

import com.ykx.shop.model.base.RspValue;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/{test}")
    @ResponseBody
    public RspValue saveUser(@PathVariable String test){
        RspValue rsp =new RspValue();
        rsp.setCode(0);
        rsp.setMsg("test");
        return rsp;
    }
}
