package com.ykx.shop.user.facade;

import com.ykx.shop.model.base.RspValue;

public interface UserFacade {

    public RspValue addUser(ReqAddUser reqAddUser);

    public RspValue getUser(String userName);


}
