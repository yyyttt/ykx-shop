package com.ykx.shop.user.facade;

import java.io.Serializable;

public class ReqAddUser implements Serializable {
    private static final long serialVersionUID = 3331560225289845693L;
    private String userName;
    private String pwd;
    private int age;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "ReqAddUser{" +
                "userName='" + userName + '\'' +
                ", pwd='" + pwd + '\'' +
                ", age=" + age +
                '}';
    }
}
