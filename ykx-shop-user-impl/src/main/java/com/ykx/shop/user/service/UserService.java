package com.ykx.shop.user.service;

import com.ykx.shop.model.Tuser;

public interface UserService {
    /**
     * 增加用户
     * @param user
     * @throws Exception
     */
    public void insertUser(Tuser user) throws Exception;

    /**
     * 更新 用户
     *
     * @param user
     * @throws Exception
     */
    public void updateUser(Tuser user) throws Exception;

    public Tuser getUserByUserName(String userName) throws Exception;
}
