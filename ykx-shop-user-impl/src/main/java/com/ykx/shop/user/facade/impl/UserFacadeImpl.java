package com.ykx.shop.user.facade.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.ykx.shop.constants.ShopConstants;
import com.ykx.shop.model.Tuser;
import com.ykx.shop.model.base.RspValue;
import com.ykx.shop.user.facade.ReqAddUser;
import com.ykx.shop.user.facade.UserFacade;
import com.ykx.shop.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
@Service(interfaceName="com.ykx.shop.user.facade.UserFacade")
public class UserFacadeImpl implements UserFacade {
    @Autowired
    private UserService userService;
    @Override
    public RspValue addUser(ReqAddUser reqAddUser) {
        RspValue rsp = new RspValue();
        try {
            Tuser user = new Tuser();
            user.setPwd(reqAddUser.getPwd());
            user.setUserName(reqAddUser.getUserName());
            user.setBalance(new BigDecimal(0));
            userService.insertUser(user);

            rsp.setCode(ShopConstants.success);
            return rsp;
        } catch (Exception e) {
            e.printStackTrace();
            rsp.setCode(ShopConstants.fail);
            return rsp;
        }
    }

    @Override
    public RspValue getUser(String userName) {
        return null;
    }
}
