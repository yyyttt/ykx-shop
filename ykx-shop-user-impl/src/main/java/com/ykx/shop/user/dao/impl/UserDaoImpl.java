package com.ykx.shop.user.dao.impl;

import com.ykx.shop.model.Tuser;
import com.ykx.shop.user.dao.UserDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
@Repository("UserDao")
public class UserDaoImpl implements UserDao {
    @Autowired
    private SqlSession sqlSession;
    @Override
    public void insertUser(Tuser user) throws Exception {
        sqlSession.insert("com.ykx.shop.user.dao.insertUser",user);
    }

    @Override
    public void updateUser(Tuser user) throws Exception {
        sqlSession.update("com.ykx.shop.user.dao.updateUser",user);
    }

    @Override
    public Tuser getUserByUserName(String userName) throws Exception {
        Map<String,String> map = new HashMap<String,String>();
        map.put("userName",userName);
        return sqlSession.selectOne("com.ykx.shop.user.dao.getUserByUserName",map);
    }
}
