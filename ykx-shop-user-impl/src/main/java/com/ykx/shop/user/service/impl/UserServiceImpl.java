package com.ykx.shop.user.service.impl;

import com.ykx.shop.model.Tuser;
import com.ykx.shop.user.dao.UserDao;
import com.ykx.shop.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDao userDao;
    @Override
    public void insertUser(Tuser user) throws Exception {
        userDao.insertUser(user);
    }

    @Override
    public void updateUser(Tuser user) throws Exception {
        userDao.updateUser(user);
    }

    @Override
    public Tuser getUserByUserName(String userName) throws Exception {
        return userDao.getUserByUserName(userName);
    }
}
