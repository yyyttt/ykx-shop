package com.ykx.shop.user;

import com.ykx.shop.model.Tuser;
import com.ykx.shop.user.dao.UserDao;
import com.ykx.shop.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class UserTest {
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserService userService;

    @Test
    public void testInsert() throws Exception {
        Tuser user = new Tuser();
        user.setAge(30);
        user.setPwd("ssssssssss");
        user.setBalance(new BigDecimal(300));
        user.setUserName("ykx");
        userDao.insertUser(user);
    }

    @Test
    public void testServiceInsert() throws Exception {
        Tuser user = new Tuser();
        user.setAge(30);
        user.setPwd("ssssssssss");
        user.setBalance(new BigDecimal(300));
        user.setUserName("ykx");
        userService.insertUser(user);
    }
}
