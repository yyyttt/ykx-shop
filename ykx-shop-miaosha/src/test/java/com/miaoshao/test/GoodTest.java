package com.miaoshao.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ykx.miaoshao.MiaoShaApplication;
import ykx.miaoshao.model.Tgood;
import ykx.miaoshao.service.GoodService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MiaoShaApplication.class})
public class GoodTest {

    @Autowired
    private GoodService goodService;
    @Autowired
    private GoodService redisGoodService;
    @Test
    public void saveGoodTest() throws  Exception{
        Tgood good = new Tgood();
        good.setAmount(100);
        good.setCode("iphone7");
        goodService.insertGood(good);
    }

    @Test
    public void buyTest() throws  Exception{
        goodService.buyGood("iphone7",7);
    }

    @Test
    public void insertGood() throws Exception{
        Tgood good = new Tgood();
        good.setAmount(100);
        good.setCode("iphone7");
        redisGoodService.insertGood(good);
    }


}
