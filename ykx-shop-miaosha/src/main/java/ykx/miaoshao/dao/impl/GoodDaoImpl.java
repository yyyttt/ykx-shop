package ykx.miaoshao.dao.impl;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ykx.miaoshao.dao.GoodDao;
import ykx.miaoshao.model.Tgood;

import java.util.HashMap;
import java.util.Map;

@Repository
public class GoodDaoImpl implements GoodDao {

    @Autowired
    private SqlSession sqlSession;

    @Override
    public void insertGood(Tgood good) throws Exception {
        sqlSession.insert("ykx.miaoshao.dao.GoodDao.insertGood",good);
    }

    @Override
    public void updateGood(Tgood good) throws Exception {
        sqlSession.update("ykx.miaoshao.dao.GoodDao.updateGood",good);
    }

    @Override
    public void deleteGood(int id) throws Exception {
        Map<String,Integer> map = new HashMap<String,Integer>();
        sqlSession.delete("ykx.miaoshao.dao.GoodDao.deleteGood",map);
    }

    @Override
    public int buyGood(String code, int buyNum) throws Exception {
        Map<String,String> map = new HashMap<String,String>();
        map.put("code",code);
        map.put("buyNum",buyNum+"");
        return sqlSession.update("ykx.miaoshao.dao.GoodDao.buyGood",map);
    }
}
