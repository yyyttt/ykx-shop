package ykx.miaoshao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import ykx.miaoshao.model.Tgood;
import ykx.miaoshao.service.GoodService;
@Service("redisGoodService")
public class GoodRedisService implements GoodService {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Override
    public void insertGood(Tgood good) throws Exception {
        redisTemplate.opsForValue().set(good.getCode(), good.getAmount() + "");
    }

    @Override
    public void updateGood(Tgood good) throws Exception {

    }

    @Override
    public void deleteGood(int id) throws Exception {
//        redisTemplate.delete(goodInfo.getCode());
    }

    @Override
    public int buyGood(String code, int buyNum) throws Exception {
        if (redisTemplate.opsForValue().increment(code, -buyNum) < 0) {
            return -1;
        }
        return 1;
    }
}
