package ykx.miaoshao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ykx.miaoshao.dao.GoodDao;
import ykx.miaoshao.model.Tgood;
import ykx.miaoshao.service.GoodService;
@Service("goodService")
@Transactional(propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000,rollbackFor=Exception.class)
public class GoodServiceImpl implements GoodService {
    @Autowired
    private GoodDao goodDao;
    @Override
    public void insertGood(Tgood good) throws Exception {
        goodDao.insertGood(good);
    }

    @Override
    public void updateGood(Tgood good) throws Exception {
        goodDao.updateGood(good);
    }

    @Override
    public void deleteGood(int id) throws Exception {
        goodDao.deleteGood(id);
    }

    @Override
    public int buyGood(String code, int buyNum) throws Exception {
        return goodDao.buyGood(code,buyNum);
    }
}
