package ykx.miaoshao.service;

import ykx.miaoshao.model.Tgood;

public interface GoodService {
    public void insertGood(Tgood good) throws Exception;

    public void updateGood(Tgood good) throws Exception;

    public void deleteGood(int id) throws Exception;

    public int buyGood(String code,int buyNum) throws Exception;
}
