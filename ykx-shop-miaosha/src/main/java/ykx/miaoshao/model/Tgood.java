package ykx.miaoshao.model;

import java.io.Serializable;

public class Tgood implements Serializable{
    private static final long serialVersionUID = -5269520752388318746L;
    private int id;
    private int amount;
    private String code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Tgood{" +
                "id=" + id +
                ", amount=" + amount +
                ", code='" + code + '\'' +
                '}';
    }
}
