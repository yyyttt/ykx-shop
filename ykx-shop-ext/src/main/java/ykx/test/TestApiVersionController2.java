package ykx.test;

import com.ykx.shop.model.base.RspValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ykx.ext.ApiVersion;

@RestController
@RequestMapping("/${version}")
@ApiVersion(2)
public class TestApiVersionController2 {
    @GetMapping("/saveUser")
    @ResponseBody
    public RspValue saveUser1(){
        RspValue rsp =new RspValue();
        rsp.setCode(0);
        rsp.setMsg("test2");
        return rsp;
    }
}
