package ykx.test;

import com.ykx.shop.model.base.RspValue;
import org.springframework.web.bind.annotation.*;
import ykx.ext.ApiVersion;

@RestController
@RequestMapping("/${version}")
@ApiVersion(1)
public class TestApiVersionController {

    @GetMapping("/saveUser")
    @ResponseBody
    public RspValue saveUser1(){
        RspValue rsp =new RspValue();
        rsp.setCode(0);
        rsp.setMsg("test1");
        return rsp;
    }


}
